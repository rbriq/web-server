package homework4;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import junit.framework.TestCase;


public class WebServerTests extends TestCase {

	private final String base = "C:\\Users\\NOONA\\Desktop\\www\\srv\\"; 
	private final String prefix = "http://127.0.0.1:8082/";
	

	final static WebServer srv = new WebServer();
	static {
		try {
			srv.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Thread(new Runnable() {
			
			public void run() {
				srv.start();
			}
		}).start();
		
	}
	
	public WebServerTests() throws Exception {
		Thread.sleep(500);
	}
	
	private String getContent(InputStream in) throws IOException {
		StringBuffer sb = new StringBuffer();
		int c;
		while ( (c = in.read()) != -1) {
			sb.append((char)c);
		}
		
		return sb.toString();
	}
	
	private String getFile(String path) throws IOException {
		
		return getContent(new FileInputStream(base+path));
	}
	
	private HttpURLConnection getConnection(String uri) throws IOException {
		URL url = new URL(prefix+uri);
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setConnectTimeout(5000);
		conn.setReadTimeout(5000);
		conn.setUseCaches(false);
		HttpURLConnection.setFollowRedirects(false);
		
		return conn;
	}
	
	
	public void test0() throws Exception {
		HttpURLConnection conn = getConnection("t0.html");
		
		String recved = getContent(conn.getInputStream());
		conn.disconnect();
		String fileContent = getFile("t0.html");
		
		assertEquals(fileContent, recved);
		
		
	}
	
	
	public void test1() throws Exception {
		
		HttpURLConnection conn = getConnection("");
		
		String recved = getContent(conn.getInputStream());
		conn.disconnect();
		String fileContent = getFile("index.html");
		
		assertEquals(fileContent, recved);
		
	}
	
	public void test2() throws Exception {
		HttpURLConnection conn = getConnection("t2.html");
		assertEquals("text/html", conn.getContentType());
		conn.disconnect();
		
		conn = getConnection("t2.htm");
		assertEquals("text/html", conn.getContentType());
		conn.disconnect();
		
		conn = getConnection("t2.txt");
		assertEquals("text/plain", conn.getContentType());
		conn.disconnect();
		
	}
	
	public void test3() throws Exception {
		HttpURLConnection conn = getConnection("dontexist.html");
		assertEquals(404, conn.getResponseCode());
		assertEquals("Not Found", conn.getResponseMessage());
		conn.disconnect();
	}
	
	public void test4() throws Exception {
		HttpURLConnection conn = getConnection("dir/t4.html");
		
		String recved = getContent(conn.getInputStream());
		conn.disconnect();
		String fileContent = getFile("dir\\t4.html");
		
		assertEquals(fileContent, recved);
	}
	
	public void test5() throws Exception {
		HttpURLConnection conn = getConnection("../forbitten.txt");
		assertNotSame(200, conn.getResponseCode());
		conn.disconnect();
	}
	public void test6() throws Exception {
		HttpURLConnection conn = getConnection("t6.tsp");
		String recved = getContent(conn.getInputStream());
		assertEquals(200, conn.getResponseCode());
		assertNotSame(getFile("t6.tsp"), recved);
		assertEquals(getFile("t6.res.txt"), recved);
		conn.disconnect();
	}
	
	public void test7() throws Exception {
		HttpURLConnection conn = getConnection("t7.tsp");
		String recved = getContent(conn.getInputStream());
		assertEquals(200, conn.getResponseCode());
		assertNotSame(getFile("t7.tsp"), recved);
		assertEquals(getFile("t7.res.txt"), recved);
		conn.disconnect();
	}
	
	public void test8() throws Exception {
		HttpURLConnection conn = getConnection("t8.tsp?i=1");
		String recved = getContent(conn.getInputStream());
		assertEquals(200, conn.getResponseCode());
		assertNotSame(getFile("t8.tsp"), recved);
		assertEquals(getFile("t8.res.txt"), recved);
		conn.disconnect();
	}
	
	public void test9() throws Exception {
		HttpURLConnection conn = getConnection("t9.tsp?x=%21");
		String recved = getContent(conn.getInputStream());
		assertEquals(200, conn.getResponseCode());
		assertNotSame(getFile("t9.tsp"), recved);
		assertEquals(getFile("t9.res.txt"), recved);
		conn.disconnect();
	}
	
	public void test10() throws Exception {
		HttpURLConnection conn = getConnection("t10.tsp");
		assertEquals(500, conn.getResponseCode());
		conn.disconnect();
	}
	
	public void test11() throws Exception {
		HttpURLConnection conn = getConnection("t11.tsp");
		assertEquals(500, conn.getResponseCode());
		conn.disconnect();
	}
	
	public void test12() throws Exception {
		HttpURLConnection conn = getConnection("t12.txt");
		assertEquals(200, conn.getResponseCode());
		assertEquals(getFile("t12.txt").length(), Integer.parseInt(conn.getHeaderField("content-length")));
		conn.disconnect();
		
		conn = getConnection("t12.tsp");
		assertEquals(200, conn.getResponseCode());
		System.out.println(getContent(conn.getInputStream()));
		assertEquals(getFile("t12.res.txt").length(), Integer.parseInt(conn.getHeaderField("content-length")));
		conn.disconnect();
		
		
	}
	
	
	public void test13() throws Exception {
		HttpURLConnection conn = getConnection("index.html");
		String date = conn.getHeaderField("date");
		SimpleDateFormat df1 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
		SimpleDateFormat df2 = new SimpleDateFormat("EEEEEEE, dd-MMM-yy HH:mm:ss 'GMT'", Locale.US);
		SimpleDateFormat df3 = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy", Locale.US);
		SimpleDateFormat df4 = new SimpleDateFormat("EEE MMM d HH:mm:ss 'IDT' yyyy", Locale.US);
		SimpleDateFormat df5 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'IDT'", Locale.US);
		SimpleDateFormat.getAvailableLocales();
		Date d = null;
		//date = "Mon, 24 May 2010 18:13:32 GMT";
		try {
			d = df1.parse(date);
			assertEquals(new Date().getDay(), d.getDay());
			return;
		} catch (ParseException e) {}
		
		try {
			d = df2.parse(date);
			assertEquals(new Date().getDay(), d.getDay());
			return;
		} catch (ParseException e) {}
		
		try {
			d = df3.parse(date);
			assertEquals(new Date().getDay(), d.getDay());
			return;
		} catch (ParseException e) {}
		
		try {
			d = df4.parse(date);
			assertEquals(new Date().getDay(), d.getDay());
			return;
		} catch (ParseException e) {}
		
		try {
			d = df5.parse(date);
			assertEquals(new Date().getDay(), d.getDay());
			return;
		} catch (ParseException e) {}
		
		assertTrue("Error parsing date: "+date, false);
	}
	
	
	public void test15() throws Exception {
		Socket s = new Socket("127.0.0.1", 8082);
		
		HttpURLConnection conn = getConnection("index.html");
		assertEquals(200, conn.getResponseCode());
		assertEquals(getFile("index.html"), getContent(conn.getInputStream()));
		conn.disconnect();
		
		PrintStream p = new PrintStream(s.getOutputStream());
		p.print("GET /t15.html HTTP/1.1\r\n");
		p.print("Host: localhost\r\n");
		p.print("\r\n");
		p.flush();
		
		String t15 = getContent(s.getInputStream());
		assertEquals(getFile("t15.html"), t15.split("\r\n\r\n")[1]);
	}
	
	
	public void test16() throws Exception {
		System.err.println("connecting");
		Socket s = new Socket("127.0.0.1", 8082);
		Thread.sleep(20000);
		PrintStream p = new PrintStream(s.getOutputStream());
		System.err.println("sending data");
		p.print("GET /index.html HTTP/1.1\r\n");
		p.print("Host: localhost\r\n");
		p.print("\r\n");
		p.flush();
		try {
			String index = getContent(s.getInputStream());
			assertTrue("server did not disconnect after 20 seconds", false);
		} catch (IOException e) {}
	}
	
	
}
