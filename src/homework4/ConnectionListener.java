package homework4;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

	/**
	 *  Connection Listener. The connection listener listens to a server socket and, 
	 *  upon an arrival of a new client connection, puts the corresponding client socket
	 *  (or an object that wraps it) in the socket queue. Then, it resumes listening 
	 *  and waiting for future connections.
	 *  @author NOONA
	 */
public class ConnectionListener implements Runnable
{
    private static ConcurrentQueue <Socket> clientSocketsQueue;
    
    /**
     * initialises the clients sockets queue with the matching size
     */
    public ConnectionListener()
    {
        clientSocketsQueue = new ConcurrentQueue <Socket>(ServerConfig.getSocketQueueSize());
    }
    
	@Override
	public void run() {
		listen();
		
	}
	/**
	 * listens to incoming connections to the server and adds them to the queue
	 */
    private void listen()
    {
    	try 
    	{
    		ServerSocket serverSocket = new ServerSocket(ServerConfig.getPortNumber());
	
	  	      while (true)
	  	      {
		  	    	Socket clientSocket = serverSocket.accept();
		  	        boolean isSocketAdded = addClientSocket(clientSocket);
		  	        if(isSocketAdded == false)
			  	    {
			  	    	Response response = new Response();
			  	    	response.setRequestError(clientSocket.getOutputStream(),"503", "queue is full");
			  	    }
		  	        //System.out.println("Connection from client " + 
		  	        	//	clientSocket.getInetAddress() +" accepted");
			    	WebServerLog.log(clientSocket, "Connection from client " + 
		  	        		clientSocket.getInetAddress() +" accepted");
			    	//WebServerLog.log(clientSocket, "number of sockets " + 
			    			//clientSocketsQueue.getQueueSize());
	  	      }
	  	   
    	}
    	catch (IOException e)
    	{
    		System.err.println("Could not listen on port: " + ServerConfig.getPortNumber() );
    		e.printStackTrace();
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    		//System.exit(1);
    	}
    }
 
    /**
     * 
     * @param clientSocket: the client socket to be added
     * @return true if clientSocket was added, false otherwise
     */
	public boolean addClientSocket(Socket clientSocket)
	{
		if(clientSocketsQueue.addElement(clientSocket) == true)
		{
			//WebServerLog.log(clientSocket, "number of sockets " + 
	    		//	clientSocketsQueue.getQueueSize());
			return true;
		}
		//WebServerLog.log(clientSocket, "number of sockets " + 
    		//	clientSocketsQueue.getQueueSize()); 
		return false;
     }
	
	/**
	 * 
	 * @return the next element (by insertion time) in the queue
	 */
	public Socket getNextClientSocket () 
	{
		return clientSocketsQueue.getNextElement();
	}
             
	 /**
     *       		 
     * @return true if sockets queue is full, false otherwise
     */	 
	public boolean isSocketQueueFull() 
	{
		return clientSocketsQueue.isQueueFull();
	}	
	
	/**
	 * 
	 * @return true if sockets queue is empty, false otherwise
	 */
	public boolean isSocketQueueEmpty() 
	{
		 return clientSocketsQueue.isQueueEmpty();
	}

}

