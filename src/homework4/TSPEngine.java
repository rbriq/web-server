package homework4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * A handler for TSP files executes the code written in the TSP file and not
 * just returns its content
 * @author NOONA
 */
public class TSPEngine implements TypeHandler {
	//private File javaFile;
	private static Integer count = 0;
	private String tspClassName = "";

	/**
	 * empty constructor
	 */
	public TSPEngine() {

	}

	@Override
	/**
	 * sets the output stream of the client for responding to the TSP page
	 * request
	 */
	public void sendHandledResponse(String absolutePath,  HttpParams params,
			PrintStream clientStream, int cnt) {
		// lock object in case several threads are reading it
		synchronized(count) 
		{
			tspClassName = "TSPRequest" + count;
			cnt = count;
			++count;
		}
		File javaFile = null;
		String javaFileName = "." + File.separator + "src" + File.separator + "homework4" + 
			File.separator + "generated" + File.separator + tspClassName + ".java";
		try {
		javaFile = new File(javaFileName);
		//System.out.println("java file: " + javaFileName);
		BufferedWriter bufferedWriter = null;
		FileInputStream requestFileStream;
	    InputStreamReader requestFileInput;
		
		bufferedWriter = new BufferedWriter(new FileWriter(javaFile));
		requestFileStream = new FileInputStream(absolutePath);
		requestFileInput = new InputStreamReader(requestFileStream);
		bufferedWriter.write("package homework4.generated;\n\n" +
				"public class " + tspClassName + " implements homework4.Tsp" + 
				"{\n" +    "java.io.PrintStream out;\n");
		bufferedWriter.write("public void sendHandledResponse(String absolutePath, "
		  		  + " homework4.HttpParams params, java.io.PrintStream clientStream, int cnt) " 
				  + "\n{\n" 
				  + "try { \n " 
				  		+ "java.io.File tempFile = new java.io.File(\".\" + java.io.File.separator + \"tempOut\" + cnt);\n" 
				  		+ "java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(tempFile);\n" 
				  + "out= new java.io.PrintStream( fileOutputStream );\n"										  
			);
		  parseTspPage(requestFileInput, bufferedWriter);
	
		  bufferedWriter.write("if(fileOutputStream != null)\n"
		  	+"{\nfileOutputStream.flush();\n fileOutputStream.close();\n}"
		  	+"if(out !=null)\n{out.flush(); \n out.close();\n}"
		  	+ "String content = readFile(cnt);\n"			  			
  			+ "int contentLength = content.length();\n"
			+ "tempFile.delete();\n"
			+ "clientStream.print(createHeaders(contentLength));\n"
			+ "clientStream.print(\"\\r\\n\" + content);\n\n"
			+ "clientStream.flush(); \n  if (clientStream!=null) clientStream.close();"
			+ "fileOutputStream.flush(); \n  if (fileOutputStream!=null) fileOutputStream.close();}"
			+ "catch (Exception e) {e.printStackTrace();\n "
			+"(new homework4.Response()).setRequestError(clientStream,\"500\", \"Error retrieving\");\n return;}\n}"
			+ "\n\nprivate String createHeaders(int contentLength) { \n"
			+ "String headersString = \"HTTP/1.1 200 OK \\r\\n\";\n"            	  
			+ "String currDate = (new homework4.Response()).getCurrentTime();\n"
			+ "String headerDate = \"Date: \" + currDate   + \"\\r\\n\";"
			+ "headersString += headerDate;\n"
			+ "\nheadersString += \"Content-Length: \" + contentLength + \"\\r\\n\""
			+ " + \"Connection: close\\r\\n\"\n"
			+ " + \"Content-Type: \" + homework4.ServerConfig.getMimeMappingValue(\"html\") + \"\\r\\n\";\n"
			+ "return headersString;\n}\n"
			+ "private String readFile(int cnt) {\n  StringBuffer  stringBuffer = new StringBuffer();\n" 
			+ "java.io.BufferedReader reader = null; \n try { \n"
			+ " reader = new java.io.BufferedReader( new  java.io.FileReader (\"tempOut\" + cnt));\n"
			+ "String line  = \"\";\n"
			+ "while( ( line = reader.readLine() ) != null ) {\n"
			+ "stringBuffer.append( line + \"\\n\" );\n}\n if(reader != null)\nreader.close(); \n}"
			+ "catch(Exception e)\n {e.printStackTrace();\n} return stringBuffer.toString();\n}\n}"
		  	);
	  	 if (bufferedWriter != null) 
	  	 {
            bufferedWriter.flush();
            bufferedWriter.close();
	  	 }
			
	   	 if (bufferedWriter != null) 
 		 {
 			 try {
 				 bufferedWriter.close();
 			 } catch (IOException e) {
 				 e.printStackTrace();
 			 }
 		  }
	   	 
	   	 if(compileSource() == false)
	   	 {
		    	(new Response()).setRequestError(clientStream,"500", "Error retrieving");
		    	return;
	   	 }
	   	Tsp tsp = null;
	  	tsp = (Tsp) (Class.forName("homework4.generated." + tspClassName).newInstance());
	  	 // invoke
	    tsp.sendHandledResponse(absolutePath, params, clientStream, cnt);
	 		}
	catch (Exception e) {
		System.out.println(e);
		e.printStackTrace();
		return;
	}
	finally
	{
		javaFile.delete();
		String classBin = "." + File.separator + "bin" + File.separator
		+ "homework4" + File.separator + "generated" + javaFileName + ".class";
		File binFile = new File(classBin);
		binFile.delete();
	}
	}
	private boolean compileSource()
	{
	  Process compileProc = null;
	  
	try {
		String command  =
	       "javac -classpath ./src src/homework4/generated/ClassName.java -d ./bin"
	       .replace("/", File.separator)
	       .replace("ClassName", tspClassName);
	
		compileProc = Runtime.getRuntime().exec(command);
	} catch (IOException e) {
		 System.out.println("Compile exit status: "
		          + compileProc.exitValue());
		      System.err.println("Compile error:" +
		    		  compileProc.getErrorStream());
		      return false;
	}
	
	  try {
		compileProc.waitFor();
	} catch (InterruptedException e) {
		 System.out.println("Compile exit status: "
		          + compileProc.exitValue());
		      System.err.println("Compile error:" +
		    		  compileProc.getErrorStream());
		      return false;
	}
	catch (Exception e) {
		 System.out.println("Compile exit status: "
		          + compileProc.exitValue());
		      System.err.println("Compile error:" +
		    		  compileProc.getErrorStream());
		      return false;
	}
	  if (compileProc.exitValue() != 0) 
	  {
		   System.out.println("Compile exit status: "
			          + compileProc.exitValue());
			      System.err.println("Compile error:" +
			    		  compileProc.getErrorStream());
			      return false;
	  }	
		return true;
	}



	/**
	* parse the tsp page content
	* 
	* @param inputStream
	*            the client input stream
	* @param bufferedWriter
	*            the buffered writer for writing to the Java translated tsp
	*            file
	*/
	public void parseTspPage(InputStreamReader inputStream,BufferedWriter bufferedWriter)
	{
		int c;
		String currString = "";
		String code = "";
		try { 
			while ((c = inputStream.read()) != -1) 
		    {
				currString += (char) c;
				currString = currString.replaceAll("\"", "\\\""); // replace " with \"
				
				if (currString.endsWith("\r\n"))
				{
					bufferedWriter.write("out.print(\"" + currString.substring(0, 
											currString.length()-2) + "\\n\");");
					bufferedWriter.write("\n");

					//bufferedWriter.flush();
					currString ="";	
					continue;
				}
				else if (currString.endsWith("\n"))
				{
					bufferedWriter.write("out.print(\"" + currString.substring(0, 
											currString.length()-1) + "\\n\");");
					bufferedWriter.write("\n");

					//bufferedWriter.flush();
					currString ="";	
					continue;
				}
				
				
				else if (currString.endsWith("<%"))
				{
					bufferedWriter.write("out.print(\"" + currString.substring(0, 
							currString.length()-2) + "\");");
						//bufferedWriter.write("\n");

					while ((c = inputStream.read()) != -1) 
					{
						code += (char) c;
						if (code.endsWith("%>")) 
						{
							bufferedWriter.write( code.substring(0, 
									code.length() - 2));
							bufferedWriter.write("\n");
							//bufferedWriter.flush();
							code = "";
							currString ="";	
							break;
						}
					}
				}
			
			}
			bufferedWriter.write("out.print(\"" + currString.substring(0, 
					currString.length()) + "\");");
			bufferedWriter.flush();
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
}

