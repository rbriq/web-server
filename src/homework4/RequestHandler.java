package homework4;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * A request-handler thread waits for a request object to arrive at the 
 * request queue. Upon an arrival of a request object, one request-handler 
 * thread processes the request by returning the required HTTP response to 
 * the client (via the corresponding client socket). The response may be the 
 * content of a file. Alternatively, the response can be generated in a 
 * different way (e.g., execution of a program) by invoking a specific-type 
 * handler.
 * @author NOONA
 */
public class RequestHandler {
	private Request request;
	OutputStream clientSocketOutputStream;
	
	/**
	 * @param request the request to be handled
	 */
	public RequestHandler(Request request) 
	{
		this.request = request;
		try 
		{
			clientSocketOutputStream = request.getClientSocket().getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * handle the request by sending appropriate response to the client
	 */
	public void handleRequest()
	{
		// If the file or directory that corresponds to path does not exist or it exists 
		//and you cannot read it
		//System.out.println("get file" + request.getFile() );
		if(request.isPathParentDir)
			new Response().setRequestNotFound(clientSocketOutputStream, request.getRequestRelativePath(), "403",
			"Permission denied");
		File requestFile = new File(request.getFile());
		if(requestFile.exists() == false)
		{
			Response response = new Response();
			response.setRequestNotFound(clientSocketOutputStream, request.getRequestRelativePath(), "404",
					"Not Found");
			return;
		}
		boolean isRequestBase = false;
		boolean isRequestDir = requestFile.isDirectory();
		String requestExtension = request.getExtension();
		//System.out.println("getBaseDir: " + ServerConfig.getBaseDir() +"\nrequest.getFile():" + request.getFile());
		if(ServerConfig.getBaseDir().contains(request.getFile()))
		{
			isRequestBase = true;
		}
		if(isExtentionSupported(requestExtension) == false && !isRequestBase && !isRequestDir)
		{
			Response response = new Response();
			response.setRequestNotFound(clientSocketOutputStream, request.getRequestRelativePath(), "403",
					"Not Supported");
			return;
		}
		// check whether this file is associated with one of the specific-type handlers
		String handlerClassName = ServerConfig.getHandlerClass(requestExtension);
		if (handlerClassName != null) // let that handler handle the request
		{
			 TypeHandler typeHandler = null;
			 try {
				 typeHandler = (TypeHandler) 
				(Class.forName("homework4." + handlerClassName).newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			typeHandler.sendHandledResponse(request.getAbsRequestPath(), request.getHttpParams(),
					new PrintStream (clientSocketOutputStream), 0);
			if(clientSocketOutputStream != null)
				try {
					clientSocketOutputStream.close();
				} catch (IOException e) {
					System.err.println(e);
					e.printStackTrace();
				}
		}
		else 
		{
			if(isRequestDir && !isRequestBase)
			{
				Response response = new Response();
				response.setRequestNotFound(clientSocketOutputStream, request.getRequestRelativePath(), "403",
					"Permission denied");
				return;
			}
			Response response = new Response(request);
			// reqesting base dir, send welcome file instead
			//System.out.println("request get file: " + request.getFile());
			if(isRequestBase)
			{	
				response.setFoundRequest(ServerConfig.getBaseDir() 
						+ ServerConfig.getWelcomeFile());
			}
			else
				response.setFoundRequest(request.getAbsRequestPath());
		}
			
			
		
	}
	/**
	 * check if extension is supported
	 * @param extension the extension to be checked
	 * @return true if supported, false otherwise
	 */
	private boolean isExtentionSupported(String extension) {
		if(extension.equals("html") || extension.equals("htm") || extension.equals("txt") 
				|| extension.equals("xml") || extension.equals("xhtml")
				|| extension.equals("tsp"))
			return true;
		return false;
	}



}
