package homework4;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Set;

/**
 * This class is used for representing request objects
 * These objects (of class Request) are obtained by parsing HTTP requests. A request 
 * object contains information such as the requested path, the GET parameters 
 * (given in the query part of the URL) and the client socket that is used for 
 * responding to the client (and properly terminating the connection).
 * @author NOONA
 */
public class Request {
	private String request = "";
	//private String requestPath = "";
	private String requestAbsPath = "";
	private String requestRelativePath = "";
	private String requestQuery = "";
	private Socket clientSocket;
	private String extension = "";
	boolean isPathParentDir = false;
	private Hashtable <String, String> queryParams;
	public Request()
	{
		
	}
	
	/**
	 * 
	 * @param request the request string
	 */
	/*public Request(String request)
	{
		this.request = request;
		queryParams = new Hashtable<String, String> ();
		this.parseRequest();
	}*/
	
	/**
	 * 
	 * @param request the request string
	 * @param clientSocket the clientSocket from which the request was received
	 */
	public Request(String request, Socket clientSocket)
	{
		this.request = request;
		queryParams = new Hashtable<String, String> ();
		this.clientSocket = clientSocket;
		this.parseRequest();
	}
	/**
	 * parses the HTTP request of this
	 */
	private void parseRequest()
	{
		//System.out.println("request as received: " + request);
		String [] splitrequest = request.split("\r\n");
	/*	System.out.println("printing split request: ");
		for(String curstr : splitrequest)
		{
			System.out.println(curstr);
		}*/
		String path = splitrequest[0].split(" ")[1];
		//System.out.println("path is :" + path);
		String baseUrl = ServerConfig.getBaseDir();
		String host = splitrequest[1].split(" ")[1];
		//System.out.println("host is: " + host);
		if(!(path.startsWith("http://")))
		{
			requestRelativePath = path;
			// if path.length is smaller than 3 then it's the root dir
			if((path.startsWith("..\\") || path.startsWith("\\..")) && path.length() > 3)
			{
				requestAbsPath = path;
				isPathParentDir = true;
			}
			else
				requestAbsPath =  baseUrl.substring(0,baseUrl.length()-1) + path;
			//System.out.println("abs path is: " + requestAbsPath);
			//System.out.println("relative path is: " + requestRelativePath);
		}
		else
		{
			path = path.substring(host.length()+("http://" + ServerConfig.getPortNumber()).length()+1);
			requestRelativePath = path;
			if(path.startsWith("..\\") || path.startsWith("\\..") && path.length() > 3)
			{
				requestAbsPath = path;
				isPathParentDir = true;
			}
			else
				requestAbsPath = baseUrl + path.substring(1);
			
			//System.out.println("http relative path is: " + requestRelativePath);
			//System.out.println("http abs path is: " + requestAbsPath);
		}
		//System.out.println("relative path is: " + requestRelativePath);
		extension = requestRelativePath.substring(requestRelativePath.lastIndexOf('.') + 1);
		//System.out.println("extension is: " + extension);
		//requestFile = requestRelativePath.substring(requestRelativePath.lastIndexOf('/'), requestRelativePath.length());
		//System.out.println("extension is: " + extension);
		if(requestRelativePath.contains("?"))
		{
			requestQuery = requestAbsPath.substring(requestAbsPath.indexOf('?')+1);
			//System.out.println("requestQuery?: " + requestQuery);
			requestRelativePath = requestRelativePath.substring(0, 
					requestRelativePath.indexOf('?'));
			//System.out.println("? relative path is: " + requestRelativePath);
			//requestAbsPath = requestAbsPath.substring(0, decodedUrlString.indexOf('?') -1 );
			requestAbsPath = baseUrl + requestRelativePath.substring(1);
			//System.out.println("? abs path is: " + requestAbsPath);
			extension = extension.substring(0,extension.indexOf('?'));
			//System.out.println("? extension is: " + extension);
			queryParams = new Hashtable<String, String> ();
			String[] queryParamsStr = requestQuery.split("&");
			for (String currParam: queryParamsStr)
			{
				//System.out.println("currParam" + currParam);
				String[] splitCurrParam = currParam.split("=");
				/*System.out.println("printing splitCurrParam: ");
				for(String curstr : splitCurrParam)
				{
					System.out.println(curstr);
				}*/
				String currValue="";
				String currParameter="";
				try {
					currValue = java.net.URLDecoder.decode(splitCurrParam[1],"UTF-8");
					currParameter = 
						java.net.URLDecoder.decode(splitCurrParam[0],"UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(queryParams.containsKey(currParameter))
					continue;
				queryParams.put(currParameter, currValue);
				//System.out.println("curr key param: " + currParameter +
					//" curr val param: " +currValue);
			}
			//System.out.println("extension is: " + extension);	
			//System.out.println("requestQuery: "  + requestQuery);
	  }
		requestAbsPath = requestAbsPath.replace("/",File.separator);
		requestRelativePath = requestRelativePath.replace("/",File.separator);
		//System.out.println("path1: " + requestAbsPath + "\n path2 : " + requestRelativePath);
		 
		
	}


	/**
	 * 
	 * @return the request relative path
	 */
	public String getRequestRelativePath()
	{
		return requestRelativePath;
	}
	public String getQueryParams()
	{
		return requestQuery;
	}
	/**
	 * 
	 * @return the request absolute path
	 */
	public String getAbsRequestPath()
	{
		return requestAbsPath;
	}
	
	/**
	 * 
	 * @return the request client socket of this
	 */
	public Socket getClientSocket()
	{
		return clientSocket;
	}
	
	/**
	 * 
	 * @return the set of query variables
	 */
	public Set <String> getQueryVariables()
	{
		return queryParams.keySet();
	}
	
	/**
	 * 
	 * @param variable variable name of an HTTP request query parameter
	 * @return the value of variable
	 */
	public  String getQueryVariableValue(String variable)
	{
		return queryParams.get(variable);
	}
	
	/**
	 * 
	 * @return the request extension
	 */
	public String getExtension()
	{
		return extension;
	}
	
	/**
	 * 
	 * @return the file name of the request
	 */
	public String getFile()
	{
		//System.out.println("get file:" + requestAbsPath);
		return requestAbsPath;
	}
	/**
	 * builds the http query params of this
	 * @return the HTTP query params of this
	 */
	HttpParams getHttpParams ()
	{
		return new HttpParams(this);
	}
	
	/**
	 * checks whether request path is a parent directory of the root directory
	 * @return true if it is, false otherwise.
	 */
	boolean isPathDir()
	{
		return isPathParentDir;
		
	}
}
