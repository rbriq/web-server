package homework4;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * TaskSocketReader waits for a client socket to arrive at the 
 * socket queue. When a socket arrives, one of these threads reads the HTTP 
 * request from the socket, parses it and creates from it a request object
 * @author NOONA
 */
public class TaskSocketReader implements Runnable{
	private ConnectionListener connectionListener;
	ConcurrentQueue <Request> requestQueue;
	
	/**
	 * 
	 * @param connectionListener: connectionListener contains the socket queue
	 * 				from which we are reading the request
	 * @param requestQueue the queue of requests that the socket 
	 * 				readers will read requests from
	 */
	TaskSocketReader (ConnectionListener connectionListener, ConcurrentQueue <Request> requestQueue)
	{
		this.connectionListener = connectionListener;
		this.requestQueue = requestQueue;
	}
	
	@Override
	public void run() 
	{
		while(true)
		{
			Request request = buildRequest();
			if(request != null)
			{
				requestQueue.addElement(request);
				WebServerLog.log(request, "request read");
			}
		}
	}
	
	/**
	 * Takes a socket from the socket queue and transforms it into a request object. 
	 * To do this it reads the HTTP request from the socket and parses it.
	 * @return the constructed request
	 */
	private Request buildRequest() 
	{
		Socket clientSocket = connectionListener.getNextClientSocket();
		if(clientSocket == null)
			return null;
		Request request = null;

		try 
		{
			clientSocket.setSoTimeout(15000);
            InputStreamReader clientInputStream = new InputStreamReader(clientSocket.getInputStream());
            int c;         
            StringBuffer requestBuffer = new StringBuffer();
            while ((c = clientInputStream.read()) != -1) 
            { 
                requestBuffer.append((char) c);
                if (requestBuffer.toString().endsWith(("\r\n\r\n")))
                	break;
            }
            request = new Request(requestBuffer.toString(), clientSocket);
		} 
		catch (Exception e) // catch any possible exception in order to keep the thread running
		{
			 
			System.err.println(e);
			try 
		        {
		            if (clientSocket != null)
		            {
	            	//if(!(clientSocket.isClosed()))
		            	{
		            		clientSocket.close();
		            	}
		            		
		            }
		        }
			 
				catch (IOException e2)
				{
					 System.err.println(e2);
					 System.err.println("here3");
				}
		}
				// e.printStackTrace();	
		return request;
	}
	
}
