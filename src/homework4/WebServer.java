package homework4;


import java.util.LinkedList;
import java.util.List;

/**
 * The class WebServer initializes and launches the server.
 * @author NOONA
 *
 */
public class WebServer {
	
	private ConnectionListener connectionListener;
	private static ConcurrentQueue <Request> requestQueue;
	private List <Thread> socketReaderThreadpool;
	private List <Thread> requestThreadpool;

    /**
     * Initializes the Web server. This includes the the initialization of the
     * server socket, queues, threads and specific-type handlers.
     */
	
	/*empty constructor*/
	public WebServer ()
	{
		
	}
	
	 /**
	  * Initializes the Web server. This includes the initialization of the
	  * server socket, queues, threads and specific-type handlers.
	  */
    public void initialize() 
    {
      	ServerConfig.parseConfig();
    	connectionListener = new ConnectionListener();
    	requestQueue = new ConcurrentQueue <Request>(ServerConfig.getRequestQueueSize());
    	socketReaderThreadpool = new LinkedList <Thread>();
    	requestThreadpool = new LinkedList <Thread>();
    	
    	for(int i = 0 ; i < ServerConfig.getSocketReaderThreadsNumber() ; i++ )
    	{
    		TaskSocketReader taskSocketReader = new TaskSocketReader(connectionListener, requestQueue); 
    		socketReaderThreadpool.add(new Thread (taskSocketReader));
    	}
    	for(int i = 0 ; i < ServerConfig.getRequestHandlerThreadsNumber() ; i++ )
    	{
    		TaskRequestHandler taskRequestHandler = new TaskRequestHandler(requestQueue); 
    		requestThreadpool.add(new Thread (taskRequestHandler));
    	}	
    };

    /**
     * Starts the server. Within this method, the threads are started
     */
    public void start() 
    {
    	Thread connectionListenerThread = new Thread(connectionListener);
    	connectionListenerThread.start();
		for (Thread socketReaderThread : socketReaderThreadpool)
		{
	        socketReaderThread.start();
		}
	    for (Thread requestHandlerThread : requestThreadpool)
	    {
	        requestHandlerThread.start();
	    } 
    };

   public static void main(String[] args) throws Exception {
    	WebServer webServer = new WebServer();
    	webServer.initialize();
    	webServer.start();
	}
	
}