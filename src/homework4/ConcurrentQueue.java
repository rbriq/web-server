package homework4;

import java.util.LinkedList;
import java.util.Queue;

/**
 *  this class implements a generic concurrent thread-safe queue
 *  @author NOONA
 */
public class ConcurrentQueue <E> {
	private Queue <E> queue;
	int size;
	
	/**
	 * 
	 * @param size
	 * 			the maximum size of the queue
	 */
	public ConcurrentQueue (int size)
	{
		queue = new LinkedList <E> ();
		this.size = size;
	}
	
	/** 
	 * add element to the queue
	 * @param element
	 * @return true if element adding is successful, false otherwise
	 */
	public synchronized boolean addElement (E element)
	{
        while (isQueueFull())
        {
     	   try 
     	   {
     		   wait(7000);
            } catch (Exception e) 
            {
         	   return false;
            }
        }
          if (isQueueFull() == false)
          {
         	 queue.add(element);
              if (queue.size() == 1)
         			 notifyAll();
              //WebServerLog.log(element, "added");
              return true;               
          }
          return false;
  }
	/**
	 * 
	 * @return the next element (by insertion time) in the queue
	 */
	
	public synchronized E getNextElement () 
	{
		E element;
        while (isQueueEmpty())
        {
        	try 
        	{
        		wait(7000);
            } 
        	catch (InterruptedException e) 
        	{
                return null;
            }
        }
        if (isQueueEmpty() == false)
        {
        	element = queue.poll();
            if (queue.size() == size-1)
            	notifyAll();
            //WebServerLog.log(element, "removed");
            return element;
        }
        return null;
	}
       /**
        * checks if queue is full    		 
        * @return true if queue is full, false otherwise
        */
		public synchronized boolean isQueueFull() 
		{
			 if(queue.size() >= size)
				 return true;
			 else
				 return false;
		 }	
	
	/**
	 * checks if queue is empty
	 * @return true if queue is empty, false otherwise
	 */
	public synchronized boolean isQueueEmpty() 
	{
		 if(queue.size() == 0)
			 return true;
		 else
			 return false;
	}
	

	/**
	 * gets current number of elements in queue
	 * @return queue size
	 */
	public synchronized int getQueueSize() 
	{
		 return queue.size();			
	}	
}

