package homework4;

/**
 * this class is used to create and start the request handler threads.
 * @author NOONA
 */
public class TaskRequestHandler implements Runnable {
	
	ConcurrentQueue <Request> requestQueue;

    public TaskRequestHandler (ConcurrentQueue <Request> requestQueue)
	{
    	this.requestQueue = requestQueue;
	}
	
	public void run() 
	{
		while(true)
		{
			Request request = requestQueue.getNextElement();
			RequestHandler requestHandler;
			if(request != null)
			{
				requestHandler = new RequestHandler(request);
				requestHandler.handleRequest();
				WebServerLog.log(request, "handled");
			}
		}	
	}
	

}
