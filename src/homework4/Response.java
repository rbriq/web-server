package homework4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * this class represents a response object that takes care of responding to a client
 * @author NOONA
 */
public class Response {
	
	private Request request;
	public Response()
	{
		
	}
	
	/** 
	 * 
	 * @param request the request to be responded to
	 */
	public Response(Request request)
	{
		this.request = request;
	}
	
	/**
	 * send the response to the request after it was found
	 */
	public void setFoundRequest(String requestPath)
	{
			//System.out.println(requestPath);
		    int c;
		    PrintStream clientPrintStream = null;
		    StringBuffer responseBuffer = new StringBuffer();
			try {
				clientPrintStream = new PrintStream (request.getClientSocket().getOutputStream());
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
            BufferedReader bufferedReader = null;
			try 
			{
				bufferedReader = new BufferedReader(new FileReader(requestPath));
				//bufferedReader = new BufferedReader(new FileReader("C://Users//NOONA//Desktop//testfolder//s.txt"));
			} catch (FileNotFoundException e2) {
				e2.printStackTrace();
			}
            try 
            {
				while ((c = bufferedReader.read()) != -1)
				{
					//System.out.println( c +" " + (char) c  );
					responseBuffer.append((char) c);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
              if (bufferedReader != null)
              {
            	  try 
            	{
            		  bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
 
              }
              String responseBody = responseBuffer.toString();
            	 
         
              String headers = "HTTP/1.1 200 OK\r\n" +
              			"Date: " + getCurrentTime() + "\r\n" +
              			"Content-Length: " + responseBody.length() + "\r\n" +
              			"Connection: close\r\n";
              			String contentType = ServerConfig.getMimeMappingValue(request.getExtension());
              if(contentType != null)
            	  headers += "Content-Type: " + contentType + "\r\n\r\n" ;
              else
            	  headers += "\r\n";
              String res = headers  + responseBody;
             // correction 1
             //clientPrintStream.print(headers + "\n\n" + responseBody);
              clientPrintStream.print(res);
              System.out.print(res);
            //  System.out.println("headers + responsebody" + headers  + responseBody);
              try 
              {
            	  clientPrintStream.flush();
      			  if(clientPrintStream != null)
      				  clientPrintStream.close();
              }
              catch (Exception e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
	}
	
	public String getCurrentTime() 
	{
		 Date date = new Date();
         TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
         SimpleDateFormat simleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z",Locale.ENGLISH);       
         return simleDateFormat.format(date);
	}

	/**
	 * send the response to the request after it was not found or isn't supported
	 * or client doesn't have permission to access resource
	 */
     public void setRequestNotFound(OutputStream clientStream, String requestPath, String statusCode, 
    		 String reason)
     {
    	 String responseHeaders =   "HTTP/1.1 " + statusCode + " " + reason +"\r\n" + "Date: " + getCurrentTime() + "\r\n";
    	 String responseBody ="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" 
    		 + "\n<html xmlns = \"http://www.w3.org/1999/xhtml\">\n<head>\n <title>" +  statusCode + " " + reason + 
    	 "</title>\n</head>\n<body>\n " + "<h1>" + reason + "</h1>\n\n<p>The requested URL " + 
    	 requestPath + " was not found on this server or " +
    		 		"isn't supported or you don't have permission to access it.</p>\n<hr />\n</body>\n</html>";
		responseHeaders += "Content-Length: " + responseBody.length() + "\r\nConnection: close\r\n" 
		+ "Content-Type: " + ServerConfig.getMimeMappingValue("html") +"\r\n";
		String responseString = responseHeaders + "\n\n" + responseBody;
		PrintStream clientPrintStream = new PrintStream(clientStream);
		clientPrintStream.print(responseString);
		try {
			clientStream.flush();
			clientPrintStream.flush();
			if(clientStream != null)
				clientStream.close();
			if(clientPrintStream != null)
			clientPrintStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (Exception e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
    	 				   
     }
     
     public void setRequestError(OutputStream clientStream, String statusCode, String reason)
     {
    	 String responseHeaders = "HTTP/1.1 " + statusCode + " Internal error" +"\r\n" + "Date: " + getCurrentTime() + "\r\n";
    	 	String responseBody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
    	 	 + "<html xmlns = \"http://www.w3.org/1999/xhtml\">\n<head>\n <title>" + statusCode +" " + reason + "</title>\n</head>\n<body>\n "
    		 + "<h1>Not Found</h1>\n\n<p>The requested URL canot be serviced at the moment, please " 
    		 +	"try again in a moment. If the problem persists, you may stop trying</p>\n<hr />\n</body>\n</html>";
		responseHeaders += "Content-Length: " + responseBody.length() + "\r\nConnection: close\r\n" 
		+ "Content-Type: text/html\r\n";
		String responseString = responseHeaders + "\n\n" + responseBody;
		PrintStream clientPrintStream = new PrintStream(clientStream);
		clientPrintStream.print(responseString);
		try {
			clientStream.flush();
			clientPrintStream.flush();
			if(clientStream != null)
				clientStream.close();
			if(clientPrintStream != null)
				clientPrintStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	 				   
     }
}
