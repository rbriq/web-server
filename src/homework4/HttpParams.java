package homework4;

import java.util.Set;

/**
 *  This class represents the parameters of an HTTP GET method received from a request
 *  @author NOONA
 */
public class HttpParams {
	private Request request;
	
	/**
	 * 
	 * @param request the request for which we want to represent the HTTP 
	 * request query parameters
	 */
	public HttpParams (Request request)
	{
		this.request = request;
	}
	/**
	 * 
	 * @return the Set of variables of the HTTP request query parameters
	 */
	public Set<String> getQueryVariables()
	{
		return request.getQueryVariables();
	}
	
	/**
	 * get the matching value of a query parameter
	 * @param variable variable name of an HTTP request query parameter
	 * @return the value of variable
	 */
	public  String get(String variable)
	{
		return request.getQueryVariableValue(variable);
	}
}
