package homework4;

import java.io.IOException;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


/**
 * Implements methods that parse the the server configuration file 
 * and return server parameterers
 * @author NOONA
 */
public class ServerConfig {

	private static String baseDir;
	private static int portNumber;
	private static int socketReaderThreadsNumber;
	private static int requestHandlerThreadsNumber;
	private static int socketQueueSize;
	private static int requestQueueSize;
	private static Hashtable <String, String> typeHandlers;
	private static String welcomeFile;
	private static Hashtable <String, String> mimeMappings;

    /**
     * parse the configuration file and assign matching parameters values
     */
    public static void parseConfig()
    {
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = null;
		Document doc = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		try {
			 doc = builder.parse("config.xml");
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		NodeList serverNodeList = doc.getElementsByTagName("server-config");
		NodeList threadsNodeList = doc.getElementsByTagName("threads");
		NodeList queuesNodeList = doc.getElementsByTagName("queues");
		NodeList welcomeFileNode = doc.getElementsByTagName("welcome-file");
		NodeList mimeMappingNodeList = doc.getElementsByTagName("mime-mapping");
		NodeList typeHandlersNodeList = doc.getElementsByTagName("type-handler");
		portNumber = Integer.parseInt(
				serverNodeList.item(0).getAttributes().getNamedItem("port").getTextContent());
		baseDir = serverNodeList.item(0).getAttributes().getNamedItem("base").getTextContent(); 
		socketReaderThreadsNumber = Integer.parseInt(
				threadsNodeList.item(0).getFirstChild().getTextContent());
		requestHandlerThreadsNumber = Integer.parseInt(
				threadsNodeList.item(0).getLastChild().getTextContent()); 
		socketQueueSize = Integer.parseInt(
				queuesNodeList.item(0).getFirstChild().getTextContent());
		requestQueueSize = Integer.parseInt(
				queuesNodeList.item(0).getLastChild().getTextContent());
		welcomeFile = welcomeFileNode.item(0).getLastChild().getTextContent();
		mimeMappings = new Hashtable<String, String> ();
		typeHandlers = new Hashtable<String, String> ();
		int typeHandlersLength = typeHandlersNodeList.getLength();
		String classValue="";
		String keyExtension="";
		for(int i = 0 ; i < typeHandlersLength ; i++ )
		{
			classValue = typeHandlersNodeList.item(i).getAttributes().item(0).getTextContent();
			int currentExtensionLength = typeHandlersNodeList.item(i).getChildNodes().getLength();
			for(int k = 0 ; k < currentExtensionLength ; k++ )
			{
				keyExtension = typeHandlersNodeList.item(i).getChildNodes().item(k).getTextContent();
				typeHandlers.put(keyExtension, classValue);
			}
			
		}
		
		int mimeMappingLength = mimeMappingNodeList.getLength();
		for(int i = 0 ; i < mimeMappingLength ; i++ )
		{
			String key = mimeMappingNodeList.item(i).getFirstChild().getTextContent();
			String value = mimeMappingNodeList.item(i).getLastChild().getTextContent();
			mimeMappings.put(key, value);
			//System.out.println("key and value from serverconfig:" + key +": " + value);
			//System.out.println("mime:" + mimeMappings.get("tsp"));
		}
    }
    
	
    /**
     * 
     * @return the base directory of the server
     */
    public static String getBaseDir()
    {
    	return baseDir;
    }
    
    /**
     * 
     * @return the port number of the server
     */
    public static int getPortNumber()
    {
    	return portNumber;
    }
    
    /**
     * 
     * @return the number of request handlers of the server
     */
    public static int getRequestHandlerThreadsNumber()
    {
    	return requestHandlerThreadsNumber;
    }
    
    /**
     * 
     * @return the number of socket readers of the server
     */
    public static int getSocketReaderThreadsNumber()
    {
    	return socketReaderThreadsNumber;
    }
    
    /**
     * 
     * @return the socket queue size of the server
     */
    public static int getSocketQueueSize()
    {
    	return socketQueueSize;
    }
    
    /**
     * 
     * @return the number of request queue size of the server
     */
    public static int getRequestQueueSize()
    {
    	return requestQueueSize;
    }
    
    /**
     * 
     * @return the welcome file name of the server
     */
    public static String getWelcomeFile()
    {
    	return welcomeFile;
    }
    
   /**
    *  return the handler class to files whose extension is the specified parameter
    * @param extension
    * @return the class that handles files with the specified extension
    */
    public static String getHandlerClass(String extension)
    {
    	if(typeHandlers.containsKey(extension))
    	{
    		return typeHandlers.get(extension);
    	}
    	else return null;  
    }


    /**
     *  return the mime mapping value of the specified parameter
     * @param extension
     * @return the mime mapping value
     */
	public static String getMimeMappingValue(String extension) 
	{		
		return mimeMappings.get(extension);
	}
    
}

