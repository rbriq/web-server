package homework4;

import java.io.PrintStream;

/**
 * The Tsp interface is used for dynamic class loading which translates TSP pages
 * @author NOONA
 */
public interface Tsp {

	public void sendHandledResponse(String absolutePath,  HttpParams params, 
			PrintStream clientStream, int cnt);
	

}
