package homework4;

import java.io.PrintStream;


/**
 * A TypeHandler interface: handles requests with certain extensions
 * should have a method that receives the necessary information for responding to a request
 * @author NOONA
 *
 */
public interface TypeHandler {

	public void sendHandledResponse(String absolutePath,  HttpParams params, PrintStream clientStream,
			int cnt);
	
}
